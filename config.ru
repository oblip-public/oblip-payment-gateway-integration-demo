# frozen_string_literal: true
require_relative 'app.rb'

run PaymentGatewayDemo::App.freeze.app
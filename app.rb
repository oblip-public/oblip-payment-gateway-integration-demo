# frozen_string_literal: true

require 'roda'
require 'json'

module PaymentGatewayDemo
  # Base class for Payment Gateway Demo Web Application
  class App < Roda
    plugin :render, engine: 'slim', views: 'views'

    public_js = [
      'nacl-fast.min.js',
      'nacl-util.min.js',
      'random-bytes.min.js'
    ]

    plugin :assets, js: public_js, path: 'assets'

    route do |routing|
      routing.assets

      routing.root do
        view :home
      end
      
      routing.on('orders') do
        routing.is do
          routing.get do
            params = routing.params

            view :order_success, locals: {
              ref: params['ref']
            }
          end

          routing.post do
            puts routing.POST['result']
            response.status = 201
            { id: 'xxxxxxxxx009' }.to_json
          end
        end
      end
    end
  end
end
